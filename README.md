# Timeline Analyzer Docker Image

[![pipeline status](https://gitlab.com/rychly-edu/docker/docker-timeline-analyzer/badges/master/pipeline.svg)](https://gitlab.com/rychly-edu/docker/docker-timeline-analyzer/commits/master)
[![coverage report](https://gitlab.com/rychly-edu/docker/docker-timeline-analyzer/badges/master/coverage.svg)](https://gitlab.com/rychly-edu/docker/docker-timeline-analyzer/commits/master)

The image is based on [nginx](https://hub.docker.com/_/nginx).
The version of the base image can be restricted on build by the `BASE_VERSION` build argument.

## Build

### The Latest Version by Docker

~~~sh
docker build --pull -t "registry.gitlab.com/rychly-edu/docker/docker-timeline-analyzer:latest" .
~~~

### All Versions by the Build Script

~~~sh
./build.sh --build "registry.gitlab.com/rychly-edu/docker/docker-timeline-analyzer" "latest"
~~~

For the list of versions to build see [docker-tags.txt file](docker-tags.txt).

## Configuration

Set environment variables. For example, see [docker-compose.yml file](docker-compose.yml).

## Run

~~~sh
docker-compose up
~~~
